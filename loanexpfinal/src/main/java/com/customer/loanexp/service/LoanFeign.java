package com.customer.loanexp.service;

import com.customer.loanexp.constants.CountriesEnums;
import com.customer.loanexp.model.request.LoanRequest;
import com.customer.loanexp.model.request.LoanRequestNew;
import com.customer.loanexp.model.request.LoanStatus;
import com.customer.loanexp.model.request.LoanUpdate;
import com.customer.loanexp.model.response.LoanResponse;
import com.customer.loanexp.model.response.LoanResponseNew;
import com.customer.loanexp.model.response.StatusResponse;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@FeignClient(name = "LoanApplication", url = "${config.rest.service.getLoanUrl}")
public interface LoanFeign {
    @PostMapping("/loan")
    public LoanResponse addLoan(@RequestBody LoanRequest loanRequest);
    @PutMapping("/loan-updation")
    public LoanResponse updateLoan(@RequestBody LoanUpdate loanUpdate);
    @PutMapping("/loan-status")
    public LoanResponse loanConfirm(@RequestBody LoanStatus loanStatus);

    //updated feign

    @GetMapping("/get-tenure")
    public ArrayList<String> getTenure(@RequestHeader("country") CountriesEnums countries, @RequestHeader("uuid") String uuid);
    @PostMapping("/loan-details")
    public LoanResponseNew loanDetails(@RequestHeader("country") CountriesEnums countries, @RequestHeader("uuid") String uuid, @RequestBody LoanRequestNew loanRequestNew);
    @PostMapping("/loan-submission")
    public StatusResponse loanSubmission(@RequestHeader("country") CountriesEnums countries, @RequestHeader("uuid") String uuid, @RequestBody LoanRequest loanRequest);

}
