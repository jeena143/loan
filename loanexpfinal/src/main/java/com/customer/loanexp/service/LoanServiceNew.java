package com.customer.loanexp.service;

import com.customer.loanexp.constants.CountriesEnums;
import com.customer.loanexp.exception.InvalidLoanAmountException;
import com.customer.loanexp.exception.InvalidTenureException;
import com.customer.loanexp.model.request.LoanRequest;
import com.customer.loanexp.model.request.LoanRequestNew;
import com.customer.loanexp.model.response.LoanResponseNew;
import com.customer.loanexp.model.response.StatusResponse;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Service;

import java.util.ArrayList;

@Service
@Slf4j
public class LoanServiceNew {
    @Autowired
    LoanFeign loanFeign;

    public ArrayList<String> getTenureExp(String uuid, CountriesEnums countries) {
        return loanFeign.getTenure(countries, uuid);

    }

    public LoanResponseNew getLoanDetailsExp(CountriesEnums countries, String uuid, LoanRequestNew loanRequestNew) {
        if (loanRequestNew.getLoanAmount() > 599 || loanRequestNew.getLoanAmount() < 2001) {
            tenureCheck(loanRequestNew.getTenure());
            return loanFeign.loanDetails(countries, uuid, loanRequestNew);
        } else {
            throw new InvalidLoanAmountException();
        }
    }

    public StatusResponse loanSubmissionExp(LoanRequest loanRequest, String uuid, CountriesEnums countries) {
        if (loanRequest.getLoanAmount() > 599 || loanRequest.getLoanAmount() < 2001) {
            tenureCheck(loanRequest.getTenure());
            return loanFeign.loanSubmission(countries, uuid, loanRequest);
        } else {
            throw new InvalidLoanAmountException();
        }
    }

















    public void tenureCheck(int tenure){

        if(tenure==12 || tenure==24  || tenure== 36 || tenure==48 || tenure==60){}
        else {
            throw new InvalidTenureException();
        }
    }
}
