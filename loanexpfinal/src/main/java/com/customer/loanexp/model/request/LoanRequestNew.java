package com.customer.loanexp.model.request;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import javax.validation.constraints.Max;
import javax.validation.constraints.Min;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanRequestNew {
    @Min(value = 600,message = "Loan amount must be in the limit of 600 JOD to 2000 JOD")
    @Max(value = 2000,message = "Loan amount must be in the limit of 600 JOD to 2000 JOD")
    private float loanAmount;
    private int tenure;
}
