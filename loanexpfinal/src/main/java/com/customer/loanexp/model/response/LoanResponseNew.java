package com.customer.loanexp.model.response;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
public class LoanResponseNew {
    private float loanAmount;
    private int tenure;
    private float installment;
    private float interest;
    private float totalInterestAmt;
    private float fee;
    private float total;

}