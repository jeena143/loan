package com.customer.loanexp.exception;

public class InvalidTenureException extends  RuntimeException{
    public InvalidTenureException(){
        super("Invalid tenure is given");
    }
}
