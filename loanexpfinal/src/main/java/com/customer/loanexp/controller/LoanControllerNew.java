package com.customer.loanexp.controller;

import com.customer.loanexp.constants.CountriesEnums;
import com.customer.loanexp.exception.InvalidTenureException;
import com.customer.loanexp.model.request.LoanRequest;
import com.customer.loanexp.model.request.LoanRequestNew;
import com.customer.loanexp.model.response.LoanResponseNew;
import com.customer.loanexp.model.response.StatusResponse;
import com.customer.loanexp.service.LoanServiceNew;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;
import java.util.ArrayList;

@RestController
@RequestMapping
public class LoanControllerNew {
    @Autowired
    LoanServiceNew loanServiceNew;

    @GetMapping("/get-tenure-exp")
    public ArrayList<String> listTenure(@RequestHeader("country") CountriesEnums countries, @RequestHeader("uuid") String uuid){
        return loanServiceNew.getTenureExp(uuid,countries);
    }
    @PostMapping("/loan-details-exp")
    public ResponseEntity<?> loanDetails(@RequestHeader("country") CountriesEnums countries, @RequestHeader("uuid") String uuid, @RequestBody @Valid LoanRequestNew loanRequestNew){
        try{
            return new ResponseEntity(loanServiceNew.getLoanDetailsExp(countries,uuid,loanRequestNew), HttpStatus.OK);
        }
        catch (InvalidTenureException i)
        {
            return new ResponseEntity(i.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }
    @PostMapping("/loan-submission-exp")
    public ResponseEntity<StatusResponse> loanSubmission(@RequestHeader("country") CountriesEnums countries, @RequestHeader("uuid") String uuid, @RequestBody @Valid LoanRequest loanRequest){
        try{
            return new ResponseEntity(loanServiceNew.loanSubmissionExp(loanRequest,uuid,countries),HttpStatus.OK);
        }
        catch (InvalidTenureException i)
        {
            return new ResponseEntity(i.getMessage(),HttpStatus.BAD_REQUEST);
        }
    }
}
