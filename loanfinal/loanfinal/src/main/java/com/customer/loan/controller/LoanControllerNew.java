package com.customer.loan.controller;

import com.customer.loan.Constants.CountriesEnums;
import com.customer.loan.model.request.LoanRequest;
import com.customer.loan.model.request.LoanRequestNew;
import com.customer.loan.model.response.LoanResponseNew;
import com.customer.loan.model.response.StatusResponse;
import com.customer.loan.service.LoanServiceNew;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;

@RestController
@RequestMapping
public class LoanControllerNew {
    @Autowired
    LoanServiceNew loanServiceNew;
    @GetMapping("/get-tenure")
    public ArrayList<String> listTenure(@RequestHeader("country")CountriesEnums countries,@RequestHeader("uuid") String uuid){
        return loanServiceNew.getTenure(uuid,countries);
    }
    @PostMapping("/loan-details")
    public ResponseEntity<LoanResponseNew> loanDetails(@RequestHeader("country") CountriesEnums countries,@RequestHeader("uuid") String uuid,@RequestBody LoanRequestNew loanRequestNew){
        return new ResponseEntity(loanServiceNew.getLoanDetails(loanRequestNew,uuid,countries), HttpStatus.OK);
    }
    @PostMapping("/loan-submission")
    public ResponseEntity<StatusResponse> loanSubmission(@RequestHeader("country") CountriesEnums countries,@RequestHeader("uuid") String uuid,@RequestBody LoanRequest loanRequest){
        return new ResponseEntity(loanServiceNew.loanSubmission(loanRequest,uuid,countries),HttpStatus.OK);
    }



}
