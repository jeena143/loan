package com.customer.loan.service;

import com.customer.loan.Constants.CountriesEnums;
import com.customer.loan.Constants.StatusEnum;
import com.customer.loan.model.entity.LoanEntity;
import com.customer.loan.model.repository.LoanRepo;
import com.customer.loan.model.request.LoanRequest;
import com.customer.loan.model.request.LoanRequestNew;
import com.customer.loan.model.response.LoanResponseNew;
import com.customer.loan.model.response.StatusResponse;
import lombok.extern.slf4j.Slf4j;
import org.modelmapper.ModelMapper;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDateTime;
import java.util.ArrayList;

import static com.customer.loan.Constants.ComputationConstant.*;
import static com.customer.loan.Constants.ComputationConstant.TWELVE_FEES;

@Service
@Slf4j
public class LoanServiceNew {
    @Autowired
    LoanRepo loanRepo;
    @Autowired
    ModelMapper mapper;
    @Autowired
    ComputationService computationService;
    public ArrayList<String> getTenure(String uuid,CountriesEnums countriesEnums) {
        ArrayList<String> tenure = new ArrayList<>();
        tenure.add("60 Months");
        tenure.add("48 Months");
        tenure.add("36 Months");
        tenure.add("24 Months");
        tenure.add("12 Months");
        return tenure;
    }

    public LoanResponseNew getLoanDetails(LoanRequestNew loanRequestNew,String uuid, CountriesEnums countriesEnums) {
        float loanInterestRate = 0;
        float loanFees = 0;
        switch (loanRequestNew.getTenure()) {
            case 60:
                loanInterestRate = SIXTY_RATE;
                loanFees = SIXTY_FEES;
                break;
            case 48:
                loanInterestRate = FORTY_RATE;
                loanFees = FORTY_FEES;
                break;
            case 36:
                loanInterestRate = THIRTY_RATE;
                loanFees = THIRTY_FEES;
                break;
            case 24:
                loanInterestRate = TWENTY_RATE;
                loanFees = TWENTY_FEES;
                break;
            case 12:
                loanInterestRate = TWELVE_RATE;
                loanFees = TWELVE_FEES;
                break;
        }
        int loanInstallment = computationService.calculateInstallment(loanRequestNew.getLoanAmount(), loanInterestRate, loanRequestNew.getTenure());
        int loanAmount = loanInstallment * loanRequestNew.getTenure();
        int loanTotalInterest = (int) Math.round(loanAmount - loanRequestNew.getLoanAmount());
        log.info(String.valueOf(loanTotalInterest) + loanRequestNew.getTenure());
        LoanResponseNew loanResponseNew = new LoanResponseNew(loanRequestNew.getLoanAmount(),loanRequestNew.getTenure(),loanInstallment, loanInterestRate, loanTotalInterest, loanFees, (loanAmount + loanFees));
        return loanResponseNew;
    }

    public StatusResponse loanSubmission(LoanRequest loanRequest,String uuid, CountriesEnums countriesEnums) {
        float loanInterestRate = 0;
        float loanFees = 0;
        switch (loanRequest.getTenure()) {
            case 60:
                loanInterestRate = SIXTY_RATE;
                loanFees = SIXTY_FEES;
                break;
            case 48:
                loanInterestRate = FORTY_RATE;
                loanFees = FORTY_FEES;
                break;
            case 36:
                loanInterestRate = THIRTY_RATE;
                loanFees = THIRTY_FEES;
                break;
            case 24:
                loanInterestRate = TWENTY_RATE;
                loanFees = TWENTY_FEES;
                break;
            case 12:
                loanInterestRate = TWELVE_RATE;
                loanFees = TWELVE_FEES;
                break;

        }
        int loanInstallment = computationService.calculateInstallment(loanRequest.getLoanAmount(), loanInterestRate, loanRequest.getTenure());
        int loanAmount = loanInstallment * loanRequest.getTenure();
        int loanTotalInterest = (int) Math.round(loanAmount - loanRequest.getLoanAmount());
        log.info(String.valueOf(loanTotalInterest) + loanRequest.getTenure());
        LoanEntity loanEntity = mapper.map(loanRequest,LoanEntity.class);
        loanEntity.setId(null);
        loanEntity.setName(loanRequest.getName());
        loanEntity.setAge(loanRequest.getAge());
        loanEntity.setLoanAmount(loanRequest.getLoanAmount());
        loanEntity.setNationalId(loanRequest.getNationalId());
        loanEntity.setSalaryRangeEnum(loanRequest.getSalaryRangeEnum());
        loanEntity.setTenure(loanRequest.getTenure());
        loanEntity.setInstallment(loanInstallment);
        loanEntity.setInterest(loanInterestRate);
        loanEntity.setTotalInterestAmt(loanTotalInterest);
        loanEntity.setFee(loanFees);
        loanEntity.setTotal(loanAmount + loanFees);
        loanEntity.setCreatedDate(LocalDateTime.now());
        loanEntity.setCreateBy(loanRequest.getName());
        loanEntity.setCountriesEnums(CountriesEnums.AE);
        loanEntity.setUuid(uuid);
        loanEntity.setStatusEnum(StatusEnum.CONFIRMED);
        LoanEntity response = loanRepo.save(loanEntity);
        return new StatusResponse(response.getId(),response.getStatusEnum());




    }
}


